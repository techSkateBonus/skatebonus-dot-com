import Head from "next/head";
import React from "react";
import Navbar from "@/components/Navbar/Navbar";
import Footer from "@/components/footer";

export const siteTitle = "SkateBonus";

const Layout: React.FC = ({ children }) => {
  return (
    <div>
      <Head>
        <title>{siteTitle}</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="description" content="The community's skate website" />
        {/* TODO: Add OG image */}
        {/* <meta
          property="og:image"
          content={`https://og-image.vercel.app/${encodeURI(
            siteTitle
          )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
        /> */}
        <meta name="og:title" content={siteTitle} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <Navbar />
      {children}
      <Footer />
    </div>
  );
};

export { Layout };

export default Layout;
