import Image from "next/image";
import { styled } from "@mui/material/styles";

const ColorOverlay = styled("div")<{ backgroundColor: string }>`
  background-color: ${(props) => props.backgroundColor};
  width: 100%;
  height: 100%;
  display: block;
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
`;

const BackgroundColor = styled("div")<{ backgroundColor: string }>`
  background-size: cover;
  background-position: center 0px;
  height: 100%;
  position: relative;
  background-color: ${(props) => props.backgroundColor};
`;

const StyledImage = styled(Image)`
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
`;

const ImageContainer = styled("div")`
  width: 100%;
  height: 100%;
  z-index: -1;
  top: 0px;
  position: absolute;
  display: block;
  overflow: hidden;
`;

export interface BackgroundProps {
  backgroundImageUrl?: string;
  imageAltText?: string;
  backgroundColor?: string;
  overlayColor?: string;
}

const Background: React.FC<BackgroundProps> = ({
  children,
  backgroundImageUrl,
  imageAltText,
  backgroundColor,
  overlayColor,
}) => {
  return (
    <>
      {backgroundColor ? (
        <BackgroundColor backgroundColor={backgroundColor}>
          {children}
        </BackgroundColor>
      ) : (
        children
      )}
      <ImageContainer>
        {backgroundImageUrl && imageAltText && (
          <StyledImage
            src={backgroundImageUrl}
            layout="fill"
            objectFit="cover"
            alt={imageAltText}
          />
        )}
        {overlayColor && <ColorOverlay backgroundColor={overlayColor} />}
      </ImageContainer>
    </>
  );
};

export { Background };

export default Background;
