import SbEditable, { SbEditableContent } from "storyblok-react";
import Grid from "./Grid";
import Feature from "./Feature";
import Page from "./Page";
import Teaser from "./Teaser";
import HomepageHero from "./homepage-hero/HomepageHero";
import { StoryblokComponent } from "storyblok-js-client";

interface DynamicComponentPropsType {
  blok: StoryblokComponent<string>;
}

interface IComponents {
  [key: string]: React.ElementType;
}

interface Props {
  blok?: SbEditableContent;
}

const Components: IComponents = {
  teaser: Teaser,
  grid: Grid,
  feature: Feature,
  page: Page,
  homepage_hero: HomepageHero,
};

const DynamicComponent: React.FC<Props> = ({ blok }) => {
  // check if component is defined above
  if (blok) {
    const componentName = blok.component;

    if (typeof Components[componentName] !== "undefined") {
      const FoundComponent = Components[componentName];
      // wrap with SbEditable for visual editing
      return (
        <SbEditable content={blok}>
          <FoundComponent blok={blok} />
        </SbEditable>
      );
    } else {
      // fallback if the component doesn't exist
      return <p>{componentName} is not yet defined.</p>;
    }
  }
  return null;
};

export default DynamicComponent;
