import DynamicComponent from "./DynamicComponent";
import { StoryblokComponent } from "storyblok-js-client";

export interface PageType {
  blok: { body: StoryblokComponent<string>[] };
}

const Page = ({ blok }: PageType) => (
  <main>
    {blok.body
      ? blok.body.map((blok: any) => (
          <DynamicComponent blok={blok} key={blok._uid} />
        ))
      : null}
  </main>
);

export default Page;
