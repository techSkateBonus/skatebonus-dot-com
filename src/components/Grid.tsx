import React from "react";
import DynamicComponent from "@/components/DynamicComponent";
import { StoryblokComponent } from "storyblok-js-client";

interface GridItemType extends StoryblokComponent<string> {
  name: string;
}

export interface GridType extends StoryblokComponent<string> {
  blok: { columns: GridItemType[] };
}

const Grid = ({ blok }: GridType) => {
  return (
    <div className="grid">
      {blok.columns.map((blok: StoryblokComponent<string>) => (
        <DynamicComponent blok={blok} key={blok._uid} />
      ))}
    </div>
  );
};

export default Grid;
