import React from "react";

export interface TeaserType {
  blok: {
    headline: string;
  };
}

const Teaser = ({ blok }: TeaserType) => <h2>{blok.headline}</h2>;

export default Teaser;
