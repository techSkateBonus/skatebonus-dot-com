import SubscribeCTA from "./subscribe-cta/SubscribeCTA";
import FooterProper from "./footer-proper/Footer";

// Pass storyblok for text and background image here.
const Footer: React.FC = () => {
  return (
    <footer>
      <SubscribeCTA />
      <FooterProper />
    </footer>
  );
};

export default Footer;
export { Footer };
