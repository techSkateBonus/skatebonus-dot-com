import Image from "next/image";
import Background from "@/components/background/Background";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarkerAlt } from "@fortawesome/free-solid-svg-icons/faMapMarkerAlt";
import { faPhoneAlt } from "@fortawesome/free-solid-svg-icons/faPhoneAlt";
import { faFacebookSquare } from "@fortawesome/free-brands-svg-icons/faFacebookSquare";
import { faTwitter } from "@fortawesome/free-brands-svg-icons/faTwitter";
import { faYoutube } from "@fortawesome/free-brands-svg-icons/faYoutube";
import { faInstagram } from "@fortawesome/free-brands-svg-icons";
import Divider from "@mui/material/Divider";
import Grid from "@mui/material/Grid";
import {
  FooterContainer,
  ContentWrapper,
  ContactAndSocialsContainer,
  SocialsWrapper,
  DividerWrapper,
  BottomContainer,
  LeftSide,
  RightSide,
} from "./Footer.style";

const FooterProper: React.FC = () => {
  return (
    <FooterContainer>
      <Background backgroundColor="#EA1361">
        <ContentWrapper>
          <Grid container justifyContent="space-evenly" alignItems="center">
            <Grid item xs={10} sm={10} md={6} lg={3} textAlign="center">
              <Link href="/">
                <a>
                  <Image
                    src="/static/images/skatebonus_logo.png"
                    width={255}
                    height={235}
                    objectFit="contain"
                    alt="SkateBonus logo"
                  />
                </a>
              </Link>
            </Grid>
            <Grid item xs={8} sm={8} md={6} lg={3} text-align="center">
              <ContactAndSocialsContainer>
                <div>
                  <FontAwesomeIcon icon={faMapMarkerAlt} /> 87 Bexley Road,
                  ArncliffeNSW Australia
                </div>
                <div>
                  <FontAwesomeIcon icon={faPhoneAlt} /> (61) 0454 1239
                </div>
                <SocialsWrapper>
                  Follow us <FontAwesomeIcon icon={faFacebookSquare} />
                  <FontAwesomeIcon icon={faTwitter} />
                  <FontAwesomeIcon icon={faYoutube} />
                  <FontAwesomeIcon icon={faInstagram} />
                </SocialsWrapper>
              </ContactAndSocialsContainer>
            </Grid>
          </Grid>
          <DividerWrapper>
            <Divider />
            <BottomContainer>
              <LeftSide>
                <a>
                  <span>ABOUT US</span>
                </a>
                <a>
                  <span>CONTACT US</span>
                </a>
                <a>
                  <span>HELP</span>
                </a>
                <a>
                  <span>PRIVACY POLICY</span>
                </a>
                <a>
                  <span>DISCLAIMER</span>
                </a>
              </LeftSide>
              <RightSide>Copyright &#169;2021 SkateBonus</RightSide>
            </BottomContainer>
          </DividerWrapper>
        </ContentWrapper>
      </Background>
    </FooterContainer>
  );
};

export default FooterProper;

export { FooterProper };
