import { styled } from "@mui/material/styles";

export const FooterContainer = styled("div")`
  position: relative;
  width: 100vw;
  height: 100%;
  flex: 1;
`;

export const ContentWrapper = styled("div")`
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin-left: auto;
  margin-right: auto;
  padding: 112px 0;
`;

export const ContactAndSocialsContainer = styled("div")`
  color: #000;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  justify-self: center;
  align-self: center;
  margin-left: auto;
  margin-right: auto;
  height: 112px;
  max-width: 300px;
  ${(props) => props.theme.breakpoints.down("sm")} {
    min-width: 100%;
  }
  ${(props) => props.theme.breakpoints.down("md")} {
    margin-bottom: 32px
  }
`;

export const SocialsWrapper = styled("div")`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const DividerWrapper = styled("div")`
  width: 90%;
  margin-left: auto;
  margin-right: auto;
`;

export const BottomContainer = styled("div")`
  color: #000;
  margin-left: auto;
  margin-right: auto;
  display: flex;
  justify-content: space-between;
  margin-top: 32px;
  ${(props) => props.theme.breakpoints.down("md")} {
    flex-direction: column;
  }
`;

export const LeftSide = styled("div")`
  display: flex;
  justify-content: center;
  text-transform: uppercase;
  align-items: center;
  & > a + a {
    margin-left: 50px;
  }
  ${(props) => props.theme.breakpoints.down("md")} {
    flex-direction: column;
    & > a {
      margin-bottom: 16px;
    }
    & > a + a {
      margin-left: 0px;
    }
  }
`;

export const RightSide = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  ${(props) => props.theme.breakpoints.down("md")} {
    margin-top: 32px;
  }
`;
