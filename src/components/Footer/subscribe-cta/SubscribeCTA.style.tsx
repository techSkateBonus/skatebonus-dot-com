import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";
import { styled } from "@mui/material/styles";
import Grid from "@mui/material/Grid";

export const BackgroundImageContainer = styled("div")`
  position: relative;
  width: 100vw;
  height: 100%;
  flex: 1;
`;

export const AlignmentContainer = styled("div")`
  display: inline-block;
  text-align: left;
  width: 100%;
`;

export const ContentContainer = styled(Grid)`
  display: flex;
  margin-left: auto;
  margin-right: auto;
  padding: 185px 0;
  justify-content: center;
`;

export const Title = styled("h2")`
  font-weight: 700;
  color: #ffffff;
  font-size: 2.6em;
  line-height: 1.2em;
  margin-block-end: 0;
  margin-block-start: 0;
  align-self: center;
  -webkit-text-stroke: 1px black;
  margin-bottom: 24px;
`;

export const SBTextField = styled(TextField)`
  & > div {
    background-color: #ffffff !important;
  }
  width: 100%;
  max-width: 420px;
  align-self: center;
`;

export const StyledFormControl = styled(FormControl)`
  flex-direction: row;
  background-color: #ffffff;
  padding: 0 24px;
`;
