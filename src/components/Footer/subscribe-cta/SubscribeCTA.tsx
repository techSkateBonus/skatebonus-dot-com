import { faPaperPlane } from "@fortawesome/free-solid-svg-icons/faPaperPlane";
import IconButton from "@mui/material/IconButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Background from "@/components/background/Background";
import {
  BackgroundImageContainer,
  AlignmentContainer,
  ContentContainer,
  Title,
  SBTextField,
  StyledFormControl,
} from "./SubscribeCTA.style";
import Grid from "@mui/material/Grid";

const staticImagePath = "/static/images/fun-skate.png";

const SubscribeCTA: React.FC = () => {
  return (
    <BackgroundImageContainer>
      {/* TODO: Make this configurable via storybloks (Through the footer component) */}
      <Background
        backgroundImageUrl={staticImagePath}
        imageAltText="dudes skateboarding - subscribe!"
        overlayColor="rgba(245, 47, 118, 0.3)"
      />
      <ContentContainer container>
        <Grid item xs={10} sm={8} md={6} lg={4} textAlign="left">
          <Title>Stay in the loop</Title>
          {/* 
            TODO: Logic (send the email address somewhere)
            TODO: Validation - make sure it's a valid and new email 
          */}
          <StyledFormControl
            sx={{ height: "10ch", width: "100%" }}
            variant="filled"
          >
            <SBTextField
              label="Your email"
              aria-roledescription="email-input"
              aria-label="email input"
              InputProps={{ disableUnderline: true }}
              variant="filled"
            />
            <IconButton
              aria-label="subscribe to SkateBonus"
              // onClick={handleClickShowPassword}
              // onMouseDown={handleMouseDownPassword}
            >
              <FontAwesomeIcon icon={faPaperPlane} />
            </IconButton>
          </StyledFormControl>
        </Grid>
      </ContentContainer>
    </BackgroundImageContainer>
  );
};

export default SubscribeCTA;
export { SubscribeCTA };
