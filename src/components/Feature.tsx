import React from "react";

export interface FeatureType {
  blok: {
    name: string;
  };
}

const Feature = ({ blok }: FeatureType) => (
  <div className="column feature">{blok.name}</div>
);

export default Feature;
