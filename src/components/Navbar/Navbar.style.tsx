import { styled } from "@mui/material/styles";
import Grid from "@mui/material/Grid";

export const LeftNav = styled(Grid)`
  display: flex;
`;

export const RightNav = styled(Grid)`
  display: flex;
  margin-left: auto;
  justify-content: center;
  align-items: center;
`;

export const ColorFlexWrapper = styled(Grid)`
  background-color: #ea1361;
  z-index: 10;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  max-height: 92px;
  padding: 0 40px;
  ${(props) => props.theme.breakpoints.down("sm")} {
    max-height: 60px;
  }
`;

export const SocialsWrapper = styled("ul")`
  list-style-type: none;
  padding-inline-start: 0;

  li + li {
    margin-left: 8px;
  }
`;

export const LinkContainer = styled("ul")`
  list-style-type: none;
  flex: 1;
  display: flex;
  justify-content: space-between;
  padding-inline-start: 3em;
  padding-inline-end: 3em;
`;

export const NavLink = styled("li")`
  display: inline-block;
  font-weight: 700;
  font-size: 1.5em;
  line-height: 1.2;
  text-transform: uppercase;
  color: #000;
`;

export const PaddingTop = styled("div")`
  height: 92px;
  ${(props) => props.theme.breakpoints.down("sm")} {
    height: 60px;
  }
`;
