export interface LinkDataType {
  label: string;
  href: string;
}

export const linkData: LinkDataType[] = [
  {
    label: "Videos",
    href: "/videos/",
  },
  {
    label: "News",
    href: "/news/",
  },
  {
    label: "Shop",
    href: "/shop/",
  },
  {
    label: "Submit",
    href: "/submit/",
  },
];

export interface SocialDataType {
  label: string;
  href: string;
}

export const socialsData: SocialDataType[] = [
  {
    label: "TikTok",
    href: "https://www.tiktok.com/@skatebonus",
  },
  {
    label: "facebook",
    href: "https://www.facebook.com/skatebonus/",
  },
  {
    label: "youtube",
    href: "https://www.youtube.com/channel/UCkbqYl-IsNh-rgXYe0oN23Q",
  },
  {
    label: "instagram",
    href: "https://www.instagram.com/skatebonus/",
  },
];
