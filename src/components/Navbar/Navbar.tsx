import Link from "next/link";
import Image from "next/image";
import {
  linkData,
  socialsData,
  SocialDataType,
  LinkDataType,
} from "./constants/data";
import {
  ColorFlexWrapper,
  LeftNav,
  LinkContainer,
  NavLink,
  RightNav,
  SocialsWrapper,
  PaddingTop,
} from "./Navbar.style";

const Navbar: React.FC = () => {
  return (
    <nav>
      <PaddingTop>
        <ColorFlexWrapper container>
          <LeftNav
            justifyContent="flex-start"
            alignItems="center"
            item
            xs={10}
            sm={8}
            md={7}
            lg={6}
          >
            <Link href="/">
              <a>
                <Image
                  src="/static/images/skatebonus_logo.png"
                  width={86}
                  height={79}
                  objectFit="fill"
                  alt="SkateBonus logo"
                />
              </a>
            </Link>
            <LinkContainer>
              {linkData.map((link: LinkDataType, i) => {
                return (
                  <NavLink key={i}>
                    <Link href={link.href}>
                      <a>{link.label}</a>
                    </Link>
                  </NavLink>
                );
              })}
            </LinkContainer>
          </LeftNav>
          <RightNav>
            <SocialsWrapper>
              {socialsData.map((social: SocialDataType, i) => {
                return (
                  <NavLink key={i}>
                    <Image
                      src={`/static/images/${social.label}-logo.svg`}
                      width={24}
                      height={24}
                      objectFit="contain"
                      alt={`${social.label} SkateBonus`}
                    />
                  </NavLink>
                );
              })}
            </SocialsWrapper>
          </RightNav>
        </ColorFlexWrapper>
      </PaddingTop>
    </nav>
  );
};

export { Navbar };

export default Navbar;
