import React from "react";
import { StoryblokComponent } from "storyblok-js-client";
import Background from "@/componentsbackground/Background";
import { StoryblokDisplayType } from "@/contentTypes/StoryblokTypes";
import { styled } from "@mui/material/styles";

export interface HomepageHeroType extends StoryblokComponent<string> {
  blok: {
    image_alt: string;
    background_display_url: StoryblokDisplayType;
    background_color: string;
    title: string;
    subtitle: string;
    overlay_color: string;
    text_color: string;
  };
}

const HeroBackgroundContainer = styled("div")`
  height: calc(100vh - 46px);
  position: relative;
  background-size: cover;
  background-position: center 0px;
`;

const ContentContainer = styled("div")`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
`;
const Sizing = styled("div")`
  ${(props) => props.theme.breakpoints.up("lg")} {
    flex-basis: 80%;
    max-width: 80%;
  }
  flex-basis: 100%;
  max-width: 100%;
`;

const ContentWrapper = styled("div")`
  box-sizing: border-box;
  max-width: 75rem;
  width: 100%;
  height: 100%;
  margin-left: auto;
  margin-right: auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
`;

const Title = styled("h1")<{ textColor?: string }>`
  font-weight: 700;
  font-size: 48px;
  ${({ textColor }) => textColor && `color: ${textColor}`}
`;

const Subtitle = styled("h2")<{ textColor?: string }>`
  font-weight: 700;
  font-size: 32px;
  ${({ textColor }) => textColor && `color: ${textColor}`}
`;

const AbsoluteDecoration = styled("div")`
  position: absolute;
  height: 180px;
  width: 180px;
  background-color: #ea1361;
  bottom: -90px;
  right: 110px;
  transition: transform 0.3s linear;
  transform-origin: center;
  ${(props) => props.theme.breakpoints.down("md")} {
    right: 30px;
    transform: scale(0.5);
    transform-origin: center;
    transition: transform 0.3s linear;
  }
`;

const HomepageHero = ({ blok }: HomepageHeroType) => {
  const {
    background_color,
    background_display_url,
    overlay_color,
    title,
    subtitle,
    // TODO: Add text color in Storyblok
    text_color,
  } = blok;
  return (
    <HeroBackgroundContainer className="column homepage-hero">
      <Background
        backgroundColor={background_color}
        backgroundImageUrl={background_display_url.filename}
        imageAltText={background_display_url.alt}
        overlayColor={overlay_color}
      >
        <ContentContainer>
          <Sizing>
            <ContentWrapper>
              {title && <Title textColor={text_color}>{title}</Title>}
              {subtitle && <Subtitle textColor={text_color}>yoghurt</Subtitle>}
            </ContentWrapper>
          </Sizing>
        </ContentContainer>
      </Background>
      <AbsoluteDecoration />
    </HeroBackgroundContainer>
  );
};

export { HomepageHero };

export default HomepageHero;
