import { createTheme } from "@mui/material";

const Theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1200,
      xl: 1536,
    },
  },
  palette: {
    primary: {
      main: "#f725b3",
    },
    text: {
      primary: "#000000",
    }
  }
  // colors: {
  //   primary: "#123456",
  
});

export { Theme };
export default Theme;
