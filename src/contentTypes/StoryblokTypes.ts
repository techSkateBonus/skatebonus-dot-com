export interface StoryblokDisplayType {
  fieldtype: string;
  id: number;
  filename: string;
  title: string;
  copyright: string;
  focus?: boolean;
  alt: string;
  name: string;
}
