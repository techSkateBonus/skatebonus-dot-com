import React from "react";
import DynamicComponent from "@/components/DynamicComponent";
import Head from "next/head";
import Storyblok, { useStoryblok } from "../lib/storyblok";
import { PageType } from "./[...slug]";
import { GetStaticProps } from "next";
import Layout from "@/componentslayout/Layout";

export default function Page({ story, preview }: PageType) {
  const enableBridge = true; // load the storyblok bridge everywhere
  // const enableBridge = preview; // enable bridge only in prevew mode
  story = useStoryblok(story, enableBridge);

  return (
    <Layout>
      <Head>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {story
        ? story.content.body.map((blok: any) => (
            <DynamicComponent blok={blok} key={blok._uid} />
          ))
        : null}
    </Layout>
  );
}

export const getStaticProps: GetStaticProps = async (context) => {
  const { params, preview } = context;
  let slug =
    params && params.slug ? (params.slug as string[]).join("/") : "home";

  let sbParams = {
    version: preview ? "draft" : "published", // or published
    cv: preview ? Date.now() : 0,
  };

  // load the stories insides the pages folder
  let { data } = await Storyblok.get(`cdn/stories/pages/${slug}`, sbParams);

  return {
    props: {
      story: data ? data.story : null,
      preview: preview || false,
    },
    revalidate: 3600, // revalidate every hour
  };
};

export async function getStaticPaths() {
  let { data } = await Storyblok.get("cdn/links/", {
    starts_with: "pages",
  });

  let paths: any[] = [];
  Object.keys(data.links).forEach((linkKey) => {
    // don't create routes for folders and the index page
    if (data.links[linkKey].is_folder || data.links[linkKey].slug === "home") {
      return;
    }

    // get array for slug because of catch all
    // remove the pages part from the slug
    const slug = data.links[linkKey].replace("pages", "");
    let splittedSlug = slug.split("/");

    paths.push({ params: { slug: splittedSlug } });
  });

  return {
    paths: paths,
    fallback: false,
  };
}
