import React from "react";
import DynamicComponent from "@/components/DynamicComponent";
import Head from "next/head";
import { StoryData } from "storyblok-js-client";
import Storyblok, { useStoryblok } from "../lib/storyblok";
import { GetStaticProps } from "next";
import Layout from "@/componentslayout/Layout";

export interface PageType {
  story: StoryData;
  preview: boolean;
}

export default function Page({ story, preview }: PageType) {
  const enableBridge = true; // load the storyblok bridge everywhere
  // const enableBridge = preview; // enable bridge only in prevew mode
  story = useStoryblok(story, enableBridge);

  return (
    <Layout>
      <Head>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {story
        ? story.content.body.map((blok: any) => (
            <DynamicComponent blok={blok} key={blok._uid} />
          ))
        : null}
    </Layout>
  );
}
export const getStaticProps: GetStaticProps = async (context) => {
  const { params, preview } = context;
  let slug =
    params && params.slug ? (params.slug as string[]).join("/") : "home";

  let sbParams = {
    version: preview ? "draft" : "published", // or published
    cv: preview ? Date.now() : 0,
  };

  let { data } = await Storyblok.get(`cdn/stories/${slug}`, sbParams);

  return {
    props: {
      story: data ? data.story : null,
      preview: preview || false,
    },
    revalidate: 3600, // revalidate every hour
  };
};

export async function getStaticPaths() {
  let { data } = await Storyblok.get("cdn/links/");

  let paths: any[] = [];
  Object.keys(data.links).forEach((linkKey) => {
    if (data.links[linkKey].is_folder || data.links[linkKey].slug === "home") {
      return;
    }

    // get array for slug because of catch all
    const slug = data.links[linkKey].slug.split("/");

    paths.push({ params: { slug: slug } });
  });

  return {
    paths: paths,
    fallback: false,
  };
}
