import type { NextPage } from "next";
import Head from "next/head";
import { GetStaticProps, GetStaticPropsContext } from "next";
import Storyblok, { useStoryblok } from "../lib/storyblok";
import DynamicComponent from "@/components/DynamicComponent";
import Layout from "@/componentslayout/Layout";

interface HomeProps {
  story: any | boolean;
  preview: boolean;
}

const Home: NextPage<HomeProps> = ({ story, preview }) => {
  story = useStoryblok(story, preview);

  return (
    <Layout>
      <Head>
        <title>SkateBonus</title>
        <meta name="description" content="The skate place for the people" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <DynamicComponent blok={story.content} />
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async (
  context: GetStaticPropsContext
) => {
  const { preview } = context;

  let slug = "home";
  let params = {
    version: preview ? "draft" : "published", // or 'published'
    cv: preview ? Date.now() : 0,
  };

  let { data } = await Storyblok.get(`cdn/stories/${slug}`, params);

  return {
    props: {
      story: data ? data.story : false,
      preview: preview || false,
    },
    revalidate: 3600, // revalidate every hour
  };
};

export default Home;
